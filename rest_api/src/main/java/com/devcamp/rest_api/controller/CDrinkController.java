package com.devcamp.rest_api.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.rest_api.model.CDrink;
import com.devcamp.rest_api.repository.CDrinkRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CDrinkController {
    @Autowired
    CDrinkRepository cDrinkRepository;

    @GetMapping("/drinksAll")
    public ResponseEntity<List<CDrink>> getAllDrink() {
        try {
            List<CDrink> cdrinks = new ArrayList<CDrink>();
            cDrinkRepository.findAll().forEach(cdrinks::add);
            return new ResponseEntity<>(cdrinks, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // lấy drink theo id
    @GetMapping("/drinks/{id}")
    public ResponseEntity<CDrink> getCVoucherById(@PathVariable("id") long id) {
        Optional<CDrink> drinks = cDrinkRepository.findById(id);
        if (drinks.isPresent()) {
            return new ResponseEntity<>(drinks.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // Tạo mới drink
    @PostMapping("/drinks")
    public ResponseEntity<Object> createCVoucher(@Valid @RequestBody CDrink cdrinks) {
        try {

            Optional<CDrink> cdrink = cDrinkRepository.findById(cdrinks.getId());
            if (cdrink.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" drink already exsit  ");
            }
            cdrinks.setNgayTao(new Date());
            cdrinks.setNgayCapNhat(null);
            CDrink drink = cDrinkRepository.save(cdrinks);
            return new ResponseEntity<>(drink, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            // Hiện thông báo lỗi tra back-end
            // return new ResponseEntity<>(e.getCause().getCause().getMessage(),
            // HttpStatus.INTERNAL_SERVER_ERROR);
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified drink: " + e.getCause().getCause().getMessage());
        }
    }

    // sửa drink theo id
    @PutMapping("/drinks/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateCVoucherById(@PathVariable("id") long id,
            @Valid @RequestBody CDrink pVouchers) {
        Optional<CDrink> cdrink = cDrinkRepository.findById(id);
        if (cdrink.isPresent()) {
            CDrink drink = cdrink.get();
            drink.setMaNuocUong(pVouchers.getMaNuocUong());
            drink.setTenNuocUong(pVouchers.getTenNuocUong());
            drink.setDonGia(pVouchers.getDonGia());
            drink.setNgayCapNhat(new Date());
            try {
                return new ResponseEntity<>(cDrinkRepository.save(drink), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified drink:" + e.getCause().getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified drink: " + id + "  for update.");
        }
    }

    // xóa drink theo id
    @DeleteMapping("/vouchers/{id}") // Dùng phương thức DELETE
    public ResponseEntity<CDrink> deleteCDrinkById(@PathVariable("id") long id) {
        try {
            cDrinkRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
