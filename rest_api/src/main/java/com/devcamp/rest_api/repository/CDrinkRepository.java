package com.devcamp.rest_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.rest_api.model.CDrink;

public interface CDrinkRepository extends JpaRepository<CDrink, Long> {

}
